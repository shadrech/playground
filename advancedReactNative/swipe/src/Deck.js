import React, { Component } from "react";
import {
  View,
  Animated,
  PanResponder,
  Dimensions,
  Text,
  LayoutAnimation,
  UIManager
} from "react-native";
import { Button, Card } from "react-native-elements";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SWIPE_THRESHOLD = 0.25 * SCREEN_WIDTH;
const SWIPE_OUT_DUR = 250;

class Deck extends Component {
  static defaultProps = {
    onSwipeRight: () => {},
    onSwipeLeft: () => {}
  }

  constructor(props) {
    super(props);

    this.state = {
      index: 0
    };

    this.position = new Animated.ValueXY();
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true, // return true if we want this panResponder to respond to user touch
      onPanResponderMove: (event, gesture) => {
        this.position.setValue({ x: gesture.dx, y: (gesture.dy * 0.1) });
      }, // called when user drags around screen
      onPanResponderRelease: (event, gesture) => {
        if (gesture.dx > SWIPE_THRESHOLD) {
          this.forceSwipe("right");
        } else if (gesture.dx < -SWIPE_THRESHOLD) {
          this.forceSwipe("left");
        } else
          this.resetPos();
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data)
      this.setState({index: 0});
  }

  componentWillUpdate() {
    UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true); // for android
    LayoutAnimation.easeInEaseOut(); // when compoent updates animate that change
  }

  forceSwipe = direction => {
    Animated.timing(this.position, {
      toValue: { x: direction === "right" ? SCREEN_WIDTH : -SCREEN_WIDTH, y: 0 },
      duration: SWIPE_OUT_DUR
    }).start(() => {
      // callback function to run after animation complete
      this.onSwipeComplete();
    });
  }

  onSwipeComplete = direction => {
    const { onSwipeLeft, onSwipeRight, data } = this.props;
    const item = data[this.index];

    direction === "right" ? onSwipeRight(item) : onSwipeLeft(item);
    this.position.setValue({ x: 0, y: 0 });
    this.setState({index: this.state.index + 1});
  }

  resetPos = () => {
    Animated.spring(this.position, {
      toValue: { x: 0, y: 0 }
    }).start();
  }

  getCardStyle = () => {
    const {position} = this;
    const rotate = position.x.interpolate({
      inputRange: [-SCREEN_WIDTH * 2, 0, SCREEN_WIDTH * 2],
      outputRange: ['-120deg', '0deg', '120deg']
    });

    return {
      ...position.getLayout(),
      transform: [{ rotate }]
    }
  }

  renderNoMoreCards = () => {
    return (
      <Card title="No more cards">
        <Text style={{marginBottom: 10}}>Cards all done!</Text>
        <Button
          title="Get more!"
          backgroundColor="red" />
      </Card>
    );
  }

  renderCards = () => {
    if (this.state.index >= this.props.data.length)
      return this.renderNoMoreCards();

    return this.props.data.map((item, i) => {
      if (i < this.state.index) return null;

      if (i === this.state.index) {
        return (
          <Animated.View
            key={item.id}
            style={[this.getCardStyle(), style.cardStyle]}
            {...this.panResponder.panHandlers}
          >
            {this.props.renderCard(item)}
          </Animated.View>
        )
      } else {
        // wrap this in a Animated.View so renderer won't have to rerender Card component from View -> Animated.View. Produces glitch side effects
        return (
          <Animated.View key={item.id} style={[style.cardStyle, {top: 10 * (i - this.state.index), transform: [{scale: 0.98}]}]}>
            {this.props.renderCard(item)}
          </Animated.View>  
        );
      }
    }).reverse();
  }

  render() {
    return (
      <View >
        {this.renderCards()}
      </View>
    );
  }
}

const style = {
  cardStyle: {
    position: 'absolute',
    width: '100%'
  }
}

export default Deck;