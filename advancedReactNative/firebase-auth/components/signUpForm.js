import React, {Component} from "react";
import {
  View,
  Text,
  ActivityIndicator
} from "react-native";
import { FormInput, FormLabel, Button } from "react-native-elements";
import axios from "axios";

export const ROOT_URL = "https://us-central1-one-time-passwords-6a91b.cloudfunctions.net";

class SignupForm extends Component {
  state = {
    value: "",
    submitting: false
  };

  handleInputChange = value => {
    console.log(value);
    this.setState({ value });
  }
  handleSubmit = async () => {
    this.setState({submitting: true});

    try {
      await axios.post(`${ROOT_URL}/createUser`, { phone: this.state.value });
      await axios.post(`${ROOT_URL}/requestOneTimePassword`, { phone: this.state.value });

      this.props.signIn();
    } catch (error) {
      this.setState({submitting: false});
      console.error(`ERROR DOING STUFF`, error);
    }
    
  }

  render() {
    return (
      <View>
        <View style={{marginBottom: 10}}>
          <FormLabel>Enter Phone Number</FormLabel>
          <FormInput onChangeText={this.handleInputChange} value={this.state.value} />
          <Button title="Submit" onPress={this.handleSubmit} disabled={this.state.submitting} />
          {this.state.submitting && <ActivityIndicator style={{marginTop: 10}} size="large" color="#0000ff" />}
        </View>
      </View>
    )
  }
}

export default SignupForm;
