import React, { Component } from "react";
import { View, Text } from "react-native";
import { FormInput, FormLabel, Button } from "react-native-elements";
import axios from "axios";
import firebase from "firebase";

import {ROOT_URL} from "./signUpForm";

class LoginForm extends Component {
  state = {
    phone: "",
    code: "",
    error: null
  };

  handlePhoneChange = phone => {
    this.setState({ phone });
  }

  handleCodeChange = code => {
    this.setState({ code });
  }

  handleSubmit = async () => {
    const {code, phone} = this.state;
    if (phone === "" || code === "") {
      this.setState({ error: "Phone and code must be provided!" });
      return;
    }

    const response = await axios.post(`${ROOT_URL}/verifyOneTimePassword`, { phone, code });
    const {token} = response.data;
    firebase.auth().signInWithCustomToken(token);
  }

  render() {
    return (
      <View>
        <View style={{marginBottom: 10}}>
          <FormLabel>Phone Number</FormLabel>
          <FormInput onChangeText={this.handlePhoneChange} value={this.state.phone} />
          <FormLabel>Code</FormLabel>
          <FormInput onChangeText={this.handleCodeChange} value={this.state.code} />
          <Button title="Submit" onPress={this.handleSubmit} />
        </View>
      </View>
    );
  }
}

export default LoginForm;