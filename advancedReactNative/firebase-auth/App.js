import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import firebase from "firebase";
import SignupForm from "./components/signUpForm";
import LoginForm from "./components/loginForm";

export default class App extends React.Component {
  state = {
    signedIn: false
  };

  componentWillMount() {
    const config = {
      apiKey: "AIzaSyCwDA99f_Ath8vSoL8hBR7wFCZQub8MCpk",
      authDomain: "one-time-passwords-6a91b.firebaseapp.com",
      databaseURL: "https://one-time-passwords-6a91b.firebaseio.com",
      projectId: "one-time-passwords-6a91b",
      storageBucket: "one-time-passwords-6a91b.appspot.com",
      messagingSenderId: "929239476309"
    };
    firebase.initializeApp(config);  
  }

  render() {
    return (
      <View style={styles.container}>
        {!this.state.signedIn ? <SignupForm signIn={() => this.setState({ signedIn: true })} /> : <LoginForm />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});
