const functions = require('firebase-functions');
const admin = require("firebase-admin");
admin.initializeApp({
  credential: admin.credential.cert(require("./service_acc.json")),
  databaseURL: "https://one-time-passwords-6a91b.firebaseio.com/"
});
const requestOneTimePassword = require("./request_one_time_password");
const verifyOneTimePassword = require("./verify_one_time_passwords");

const createUser = require("./create_user");

exports.createUser = functions.https.onRequest(createUser);

exports.requestOneTimePassword = functions.https.onRequest(requestOneTimePassword);

exports.verifyOneTimePassword = functions.https.onRequest(verifyOneTimePassword);
