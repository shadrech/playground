const admin = require("firebase-admin");

module.exports = (req, res) => {
  if (!req.body.phone || !req.body.code)
    return res.status(422).send({ err: "Phone and code required!" });

  const phone = String(req.body.phone).replace(/[^\d]/g, "");
  const code = parseInt(req.body.code);

  admin.auth().getUser(phone)
    .then(userRecord => {
      const ref = admin.database().ref(`users/${phone}`);
      ref.on("value", snapshot => {
        console.log(snapshot);
        ref.off(); // delete event handler listening to references. "Stop listening to other ref updates"
        const user = snapshot.val();
        if (user.code !== code || !user.codeValid)
          return res.status(422).send({ error: "Code not valid" });

        return res.send("Code valid");
      });

      ref.update({ codeValid: false });
      return admin.auth().createCustomToken(phone)
        .then(token => res.send({ token }));
    })
    .catch(error => res.status(422).send({ error }));
}
