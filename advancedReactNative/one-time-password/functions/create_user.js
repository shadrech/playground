const admin = require("firebase-admin");

module.exports = (req, res) => {
  const {body} = req;
  // verify user provided phone
  if (!body.phone)
    return res.status(422);

  //format number to remove dashes, parens...
  const phone = String(body.phone).replace(/[^\d]/g, "");
  //create new user acc
  admin.auth().createUser({ uid: phone })
    .then(user => res.send(user)) // respond to user saying acc was created
    .catch(err => {
      console.error("ERROR CREATING USER:", err);
      res.status(422).send(err);
    });
}
