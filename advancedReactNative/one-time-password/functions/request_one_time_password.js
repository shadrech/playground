const admin = require("firebase-admin");
const twilio = require("./twilio");

module.exports = (req, res) => {
  let phone = req.body.phone;
  if (!phone)
    return res.status(422).send({ error: "You must provide a phone number" });

  phone = String(phone).replace(/[^\d]/g, "");

  return admin.auth().getUser(phone)
    .then(userRecord => {
      const code = Math.floor(Math.random() * 8999 + 1000);
      twilio.messages.create({
        body: `Your code is ${code}`,
        to: phone,
        from: "+441494372257"
      }, err => {
        if (err) {
          console.error("ERROR SENDING TWILIO MSSG", err);
          return res.status(422).json(err);
        }
        
        admin.database().ref(`users/${phone}`)
          .update({ code, codeValid: true }, () => {
            res.send({ success: true })
          });
      });
      return res.send({ code, userRecord });
    })
    .catch(err => {
      console.error(err);
      res.status(422).json(err);
    })
}